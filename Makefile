CXXFLAGS = -O3 -std=c++17 -Iinclude

OUTPUTS = FieldMgrBase.ok.o FieldMgrBase.bad.o

report: $(OUTPUTS)
	@for f in $^ ; do echo "===== $$f =====" ; objdump -t -C $$f | grep release ; done

FieldMgrBase.ok.o: src/FieldMgrBase.cpp
	$(CXX) $(CXXFLAGS) -DGOOD_CASE -c -o $@ $<

FieldMgrBase.bad.o: src/FieldMgrBase.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

asm: $(OUTPUTS:.o=.asm)

%.asm: %.o
	objdump -D --no-addresses --special-syms -C $< > $@

clean:
	$(RM) $(OUTPUTS) $(OUTPUTS:.o=.asm)
