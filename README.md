This is a small test bench that demonstrates an issue observed recently where
for no apparent reason the compiler believes it needs an external
```
non-virtual thunk to ToolHandle<GiGaFactoryBase<G4MagIntegratorStepper, G4Mag_EqRhs*> >::release(GiGaFactoryBase<G4MagIntegratorStepper, G4Mag_EqRhs*>*) const
```
only when a slightly related function uses a `__builtin_expect`.

To see the effect it is enough to run
```
make
```
with gcc >= 11.1
